import Link from 'next/link';
import Layout from '../components/MyLayout';
import fetch from 'isomorphic-unfetch';

const PostLink = (props: {id: number, name: string}) => (
    <li>
        <Link href={`/p/[id]`} as={`/p/${props.id}`} >
            <a>{props.name}</a>
        </Link>
        <style jsx>{`
          li {
            list-style: none;
            margin: 5px 0;
          }

          a {
            text-decoration: none;
            color: blue;
            font-family: 'Arial';
          }

          a:hover {
            opacity: 0.6;
          }
        `}</style>
    </li>
);

const Index = (props: {shows: any[]}) => (
    <Layout>
        <h1>batman tv show</h1>
        <ul>
            {
                props.shows.map((item: any) => (
                    <PostLink key={item.id} id={item.id} name={item.name}/>
                ))
            }
        </ul>
        <style jsx>{`
            h1, a {
              font-family: 'Arial';
            }

            ul {
              padding: 100;
            }
        `}
        </style>
    </Layout>
);

Index.getInitialProps = async function () {
    const result = await fetch('https://api.tvmaze.com/search/shows?q=batman');
    const data = await result.json();
    // console.log('--- got data ', data);

    return {
        shows: data.map((item: any) => item.show)
    }
};

export default Index;
