import Layout from '../components/MyLayout'

const About = () => {
    return (
        <Layout>
            <h2>About page</h2>
        </Layout>
    );
};

export default About;
